
public interface IUser {
	
	void setLastName(String value);
	String getLastName();

	void setFirstName(String value);
	String getFirstName();

	void setMiddleName(String value);
	String getMiddleName();

	void setAddress(String value);
	String getAddress();

	void setBirthDate(String value);
	String getBirthDate();

	void setContactNumber(String value);
	String getContactNumber();

}
