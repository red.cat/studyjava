import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class FrameWithdraw extends JFrame {

	private JPanel contentPane;
	private JTextField txt_account_number;
	private JTextField txt_pin_number;
	private JLabel lbl_current_balance;
    private JLabel lblYourCurrentBalance;
    BankAccount bank_account;
    SavingsAccount savings_account;
    private JTextField txt_withdraw_amount;
    private JLabel lbl_new_balance;
    private JLabel lblYourNewBalance;;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameWithdraw frame = new FrameWithdraw();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameWithdraw() {
		setTitle("ATM Machine: Withdraw");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 506, 373);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAccountDetails = new JLabel("Account Details");
		lblAccountDetails.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAccountDetails.setBounds(12, 13, 165, 16);
		contentPane.add(lblAccountDetails);
		
		JLabel lblAccountNumber = new JLabel("Account Number:");
		lblAccountNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAccountNumber.setBounds(22, 42, 111, 16);
		contentPane.add(lblAccountNumber);
		
		JLabel lblPinNumber = new JLabel("Pin Number:");
		lblPinNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPinNumber.setBounds(32, 68, 101, 16);
		contentPane.add(lblPinNumber);
		
		txt_account_number = new JTextField();
		txt_account_number.setBounds(145, 39, 194, 22);
		contentPane.add(txt_account_number);
		txt_account_number.setColumns(10);
		
		txt_pin_number = new JTextField();
		txt_pin_number.setBounds(145, 65, 194, 22);
		contentPane.add(txt_pin_number);
		txt_pin_number.setColumns(10);
		
		JLabel lblYourCurrentBalance = new JLabel("Your Current Balance:");
		lblYourCurrentBalance.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblYourCurrentBalance.setBounds(38, 141, 154, 29);
		contentPane.add(lblYourCurrentBalance);
		lblYourCurrentBalance.setVisible(false);
		
		lbl_current_balance = new JLabel("New label");
		lbl_current_balance.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_current_balance.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbl_current_balance.setBounds(204, 147, 171, 16);
		contentPane.add(lbl_current_balance);
		lbl_current_balance.setVisible(false);
		
		JButton btn_go_back_to_main = new JButton("Back to Main Menu");
		btn_go_back_to_main.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int confirmed = JOptionPane.showConfirmDialog(null, "Are you sure you want to go back to the main menu?", "Confirmation Dialog", JOptionPane.YES_NO_OPTION);
				
				if (confirmed == JOptionPane.YES_OPTION) 
				{
					goBackToMain();
				}
			}
		});
		btn_go_back_to_main.setBounds(12, 288, 165, 25);
		contentPane.add(btn_go_back_to_main);
		
		JButton btn_validate = new JButton("Validate");
		btn_validate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String account_number = txt_account_number.getText();
				String pin_number = txt_pin_number.getText();
				
				bank_account = Globals.validateForBankAccount(account_number, pin_number);
				savings_account = Globals.validateForSavingsAccount(account_number, pin_number);
				
				if (bank_account != null) {
					lbl_current_balance.setText("Php " + String.valueOf(bank_account.balance));
					lbl_current_balance.setVisible(true);
					lblYourCurrentBalance.setVisible(true);
				}
				else if (savings_account != null) {
					lbl_current_balance.setText("Php " + String.valueOf(savings_account.balance));
					lbl_current_balance.setVisible(true);
					lblYourCurrentBalance.setVisible(true);
				}
				else {
					showMessage("Account not found");
					return;
				}
			}
		});
		btn_validate.setBounds(351, 64, 97, 25);
		contentPane.add(btn_validate);
		
		txt_withdraw_amount = new JTextField();
		txt_withdraw_amount.setColumns(10);
		txt_withdraw_amount.setBounds(145, 194, 194, 22);
		contentPane.add(txt_withdraw_amount);
		
		JLabel lblDepositAmount = new JLabel("Withdraw Amount:");
		lblDepositAmount.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDepositAmount.setBounds(12, 197, 121, 16);
		contentPane.add(lblDepositAmount);
		
		JButton btn_withdraw = new JButton("Withdraw");
		btn_withdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// before withdrawing, check first if more than Php 100
				double withdraw_value = 0;
				if ( ! txt_withdraw_amount.getText().isEmpty()) {
					withdraw_value = Double.parseDouble(txt_withdraw_amount.getText());
				}
				else
				{
					showMessage("Please input a valid account first");
					return;
				}
				
				// check if any selected account is existing
				if ( bank_account != null || savings_account != null ) {
						// if bank account existing
						if (bank_account != null) {
							
							bank_account.withdraw(withdraw_value);
							lblYourNewBalance.setVisible(true);

							// reinitialize display texts
							lbl_new_balance.setText(String.valueOf(bank_account.getBalance()));
							lbl_new_balance.setVisible(true);
						}
						else
						{
							// if savings account existing
							savings_account.withdraw(withdraw_value);
							lblYourNewBalance.setVisible(true);

							// reinitialize display texts
							lbl_new_balance.setText(String.valueOf(savings_account.getBalance()));
							lbl_new_balance.setVisible(true);
						}
					}
				else
				{
					showMessage("Please input a valid account first");
					return;
				}

			}
		});
		btn_withdraw.setBounds(351, 193, 97, 25);
		contentPane.add(btn_withdraw);
		
		lblYourNewBalance = new JLabel("Your New Balance:");
		lblYourNewBalance.setHorizontalAlignment(SwingConstants.RIGHT);
		lblYourNewBalance.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblYourNewBalance.setBounds(32, 243, 145, 16);
		lblYourNewBalance.setVisible(false);
		contentPane.add(lblYourNewBalance);
		
		lbl_new_balance = new JLabel("New label");
		lbl_new_balance.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_new_balance.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbl_new_balance.setBounds(223, 243, 154, 16);
		lbl_new_balance.setVisible(false);
		contentPane.add(lbl_new_balance);
	}

	public void goBackToMain() {
		MainMenu mainMenu = new MainMenu();
		dispose();
	}
	
	public void showMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}
}