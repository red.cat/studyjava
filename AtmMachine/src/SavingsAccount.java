
public class SavingsAccount extends BankAccount implements IBankAccount {

	String account_status;
	String account_number;
    String pin_number;
    double balance;
    User user;
    static final double interest_rate = 0.05;
    static final double maintaining_balance = 5000;

	public SavingsAccount(String account_number, String pin_number, User user, double balance) {
		super(account_number, pin_number, user);
		// TODO Auto-generated constructor stub
    	this.account_status = "Open";
    	this.account_number = account_number;
    	this.pin_number = pin_number;
    	this.balance = balance;
    	this.user = user;
	}
	
	@Override
	public void deposit(double value) {
		if ( value < 100) {
			this.showMessage("Input an amount higher than Php 100.00");
			return;
		}

		this.balance += value;
		double interest = this.balance * interest_rate;
		this.balance += interest;
	}
	
    public String getAccountStatus() {
    	return this.account_status;
    }

    public String getAccountNumber() {
    	return this.account_number;
    }

    public String getPinNumber() {
    	return this.pin_number;
    }
	
    public double getBalance() {
    	return this.balance;
    }

	@Override
	public void withdraw(double value) {

		if ( value < 100 ) {
			this.showMessage("Input an amount higher than Php 100.00");
			return;
		}
		
		if ( value > this.balance ) {
			this.showMessage("Input an amount less than your current balance");
			return;
		}

		if ( (this.balance - value) < maintaining_balance ) {
			this.showMessage("Input an amount that wouldn't go less than the maintaining balance");
			return;
		}

		this.balance -= value;
	}
	
	public SavingsAccount closeAccount() {
		this.balance = 0;
		this.account_status = "Closed";
		return this;
	}
}
