import java.util.ArrayList;
import java.util.Random;

public class Globals {
	
	public static ArrayList<String> account_numbers = new ArrayList<String>();
	public static ArrayList<BankAccount> bank_accounts = new ArrayList<BankAccount>();
	public static ArrayList<SavingsAccount> savings_accounts = new ArrayList<SavingsAccount>();
	
	public static String generateAccountNumber() {
		Random random = new Random();
		String account_number = "";
		while(true) {
			// break only when generated account_number isn't existing in the ArrayList account_numbers
	
			String randomFourDigit = String.format("%04d", random.nextInt(10000));
		    if(randomFourDigit != null)
		    {
		    	if( ! account_numbers.contains(randomFourDigit) )
		    	{
		    		account_number = randomFourDigit;
		    		break;
		    	}
		    }

		}
		return account_number;
	}
	
	

	
	public static BankAccount addBankAccount(BankAccount account) {
		account_numbers.add(account.account_number);
		bank_accounts.add(account);
		return account;
	}
	
	public static SavingsAccount addSavingsAccount(SavingsAccount account) {
		account_numbers.add(account.account_number);
		savings_accounts.add(account);
		return account;
	}
	
	public static BankAccount validateForBankAccount(String account_number, String pin_number) {
		BankAccount foundAccount = null;

		for(BankAccount account : bank_accounts) {
			
			// found an account~!
			if (account.getAccountNumber().equalsIgnoreCase(account_number) 
					&& account.getPinNumber().equalsIgnoreCase(pin_number))
			{
				foundAccount = account;
				break;
			}	
		}
		return foundAccount;
	}
	
	public static SavingsAccount validateForSavingsAccount(String account_number, String pin_number) {
		SavingsAccount foundAccount = null;

		for(SavingsAccount account : savings_accounts) {
			// found an account~!
			if (account.getAccountNumber().equalsIgnoreCase(account_number) 
					&& account.getPinNumber().equalsIgnoreCase(pin_number))
			{
				foundAccount = account;
				break;
			}
		}
		return foundAccount;
	}
}
