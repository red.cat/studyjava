
public class User implements IUser {
    String last_name;
    String first_name;
    String middle_name;
    String address;
    String birth_date;
    String contact_number;

    @Override
    public void setLastName(String value) { this.last_name = value; }
    @Override
    public String getLastName() { return this.last_name; }

    @Override
    public void setFirstName(String value) { this.first_name = value; }
    @Override
    public String getFirstName() { return this.first_name; }

    @Override
    public void setMiddleName(String value) { this.middle_name = value; }
    @Override
    public String getMiddleName() { return this.middle_name; }

    @Override
    public void setAddress(String value) { this.address = value; }
    @Override
    public String getAddress() { return this.address; }

    @Override
    public void setBirthDate(String value) { this.birth_date = value; }
    @Override
    public String getBirthDate() { return this.birth_date; }

    @Override
    public void setContactNumber(String value) { this.contact_number = value; }
    @Override
    public String getContactNumber() { return this.contact_number; }

}
