import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class FrameCloseAccount extends JFrame {

	private JPanel contentPane;
	private JTextField txt_last_name;
	private JTextField txt_first_name;
	private JTextField txt_middle_name;
	private JTextField txt_address;
	private JTextField txt_birth_date;
	private JTextField txt_contact_number;
	private JTextField txt_account_number_display;
	private JTextField txt_account_number;
	private JTextField txt_pin_number;
	private JTextField txt_type_of_account;
	private JTextField txt_account_status;
	private BankAccount selected_bank_account;
	private SavingsAccount selected_savings_account;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameCloseAccount frame = new FrameCloseAccount();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameCloseAccount() {
		setTitle("ATM Machine: Close Account");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 398, 615);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblClientProfiel = new JLabel("Client Profile");
		lblClientProfiel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblClientProfiel.setBounds(12, 82, 138, 16);
		contentPane.add(lblClientProfiel);
		
		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLastName.setBounds(22, 122, 108, 16);
		contentPane.add(lblLastName);
		
		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFirstName.setBounds(22, 151, 108, 16);
		contentPane.add(lblFirstName);
		
		JLabel lblMiddleName = new JLabel("Middle Name:");
		lblMiddleName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMiddleName.setBounds(22, 180, 108, 16);
		contentPane.add(lblMiddleName);
		
		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAddress.setBounds(22, 215, 108, 16);
		contentPane.add(lblAddress);
		
		JLabel lblBirthDate = new JLabel("Birth Date:");
		lblBirthDate.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBirthDate.setBounds(22, 244, 108, 16);
		contentPane.add(lblBirthDate);
		
		JLabel lblContactNumber = new JLabel("Contact Number");
		lblContactNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		lblContactNumber.setBounds(22, 273, 108, 16);
		contentPane.add(lblContactNumber);
		
		txt_last_name = new JTextField();
		txt_last_name.setEditable(false);
		txt_last_name.setBounds(142, 119, 216, 22);
		contentPane.add(txt_last_name);
		txt_last_name.setColumns(10);
		
		txt_first_name = new JTextField();
		txt_first_name.setEditable(false);
		txt_first_name.setBounds(142, 148, 216, 22);
		contentPane.add(txt_first_name);
		txt_first_name.setColumns(10);
		
		txt_middle_name = new JTextField();
		txt_middle_name.setEditable(false);
		txt_middle_name.setBounds(142, 177, 216, 22);
		contentPane.add(txt_middle_name);
		txt_middle_name.setColumns(10);
		
		txt_address = new JTextField();
		txt_address.setEditable(false);
		txt_address.setBounds(142, 212, 216, 22);
		contentPane.add(txt_address);
		txt_address.setColumns(10);
		
		txt_birth_date = new JTextField();
		txt_birth_date.setEditable(false);
		txt_birth_date.setBounds(142, 241, 216, 22);
		contentPane.add(txt_birth_date);
		txt_birth_date.setColumns(10);
		
		txt_contact_number = new JTextField();
		txt_contact_number.setEditable(false);
		txt_contact_number.setBounds(142, 270, 216, 22);
		contentPane.add(txt_contact_number);
		txt_contact_number.setColumns(10);
		
		JLabel lblAccountDetails = new JLabel("Account Details");
		lblAccountDetails.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAccountDetails.setBounds(12, 311, 138, 16);
		contentPane.add(lblAccountDetails);
		
		JLabel lblAccountNumber = new JLabel("Account Number:");
		lblAccountNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAccountNumber.setBounds(22, 376, 108, 16);
		contentPane.add(lblAccountNumber);
		
		JLabel lblCurrentBalance = new JLabel("Current Balance:");
		lblCurrentBalance.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCurrentBalance.setBounds(22, 452, 108, 16);
		contentPane.add(lblCurrentBalance);
		
		txt_account_number_display = new JTextField();
		txt_account_number_display.setEditable(false);
		txt_account_number_display.setColumns(10);
		txt_account_number_display.setBounds(142, 373, 216, 22);
		contentPane.add(txt_account_number_display);
		
		JLabel lbl_current_balance = new JLabel("");
		lbl_current_balance.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbl_current_balance.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_current_balance.setBounds(182, 443, 176, 37);
		contentPane.add(lbl_current_balance);
		
		JButton btn_back_to_main = new JButton("Back to Main Menu");
		btn_back_to_main.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int confirmed = JOptionPane.showConfirmDialog(null, "Are you sure you want to go back to the main menu?", "Confirmation Dialog", JOptionPane.YES_NO_OPTION);
				
				if (confirmed == JOptionPane.YES_OPTION) 
				{
					goBackToMain();
				}
			}
		});
		btn_back_to_main.setBounds(12, 530, 159, 25);
		contentPane.add(btn_back_to_main);
		
		JLabel lblAccountNumber_1 = new JLabel("Account Number:");
		lblAccountNumber_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAccountNumber_1.setBounds(22, 13, 108, 16);
		contentPane.add(lblAccountNumber_1);
		
		txt_account_number = new JTextField();
		txt_account_number.setBounds(142, 10, 216, 22);
		contentPane.add(txt_account_number);
		txt_account_number.setColumns(10);
		
		JLabel lblPinNumber = new JLabel("Pin Number:");
		lblPinNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPinNumber.setBounds(32, 42, 98, 16);
		contentPane.add(lblPinNumber);
		
		txt_pin_number = new JTextField();
		txt_pin_number.setBounds(142, 39, 216, 22);
		contentPane.add(txt_pin_number);
		txt_pin_number.setColumns(10);
		
		JButton btn_validate = new JButton("Validate");
		btn_validate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String account_number = txt_account_number.getText();
				String pin_number = txt_pin_number.getText();
				
				BankAccount bank_account = Globals.validateForBankAccount(account_number, pin_number);
				SavingsAccount savings_account = Globals.validateForSavingsAccount(account_number, pin_number);
				
				if (bank_account != null) {
					
					selected_bank_account = bank_account;
					txt_last_name.setText(bank_account.user.getLastName());
					txt_first_name.setText(bank_account.user.getFirstName());
					txt_middle_name.setText(bank_account.user.getMiddleName());
					txt_address.setText(bank_account.user.getAddress());
					txt_birth_date.setText(bank_account.user.getBirthDate());
					txt_contact_number.setText(bank_account.user.getContactNumber());
					txt_account_status.setText(bank_account.getAccountStatus());
					txt_account_number_display.setText(bank_account.getAccountNumber());
					
					// setting balance
					lbl_current_balance.setText("Php " + String.valueOf(bank_account.balance));
					lbl_current_balance.setVisible(true);
					txt_type_of_account.setText("Bank Account");

				}
				else if (savings_account != null) {

					selected_savings_account = savings_account;
					txt_last_name.setText(savings_account.user.getLastName());
					txt_first_name.setText(savings_account.user.getFirstName());
					txt_middle_name.setText(savings_account.user.getMiddleName());
					txt_address.setText(savings_account.user.getAddress());
					txt_birth_date.setText(savings_account.user.getBirthDate());
					txt_contact_number.setText(savings_account.user.getContactNumber());
					txt_account_status.setText(savings_account.getAccountStatus());
					txt_account_number_display.setText(savings_account.getAccountNumber());

					// setting balance
					lbl_current_balance.setText("Php " + String.valueOf(savings_account.balance));
					lbl_current_balance.setVisible(true);
					txt_type_of_account.setText("Savings Account");
				}
				else {
					showMessage("Account not found");
					return;
				}
			}
		});
		btn_validate.setBounds(220, 74, 138, 25);
		contentPane.add(btn_validate);
		
		txt_type_of_account = new JTextField();
		txt_type_of_account.setEditable(false);
		txt_type_of_account.setBounds(140, 408, 218, 22);
		contentPane.add(txt_type_of_account);
		txt_type_of_account.setColumns(10);
		
		JLabel lblTypeOfAccount = new JLabel("Type of Account:");
		lblTypeOfAccount.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTypeOfAccount.setBounds(12, 411, 118, 16);
		contentPane.add(lblTypeOfAccount);
		
		JLabel lblAccountStatus = new JLabel("Account Status:");
		lblAccountStatus.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAccountStatus.setBounds(22, 340, 108, 16);
		contentPane.add(lblAccountStatus);
		
		txt_account_status = new JTextField();
		txt_account_status.setEditable(false);
		txt_account_status.setColumns(10);
		txt_account_status.setBounds(142, 338, 216, 22);
		contentPane.add(txt_account_status);
		
		JButton btn_close_account = new JButton("Close Account");
		btn_close_account.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int confirmed = JOptionPane.showConfirmDialog(null, "Are you sure to close this account?", "Confirmation Dialog", JOptionPane.YES_NO_OPTION);
				
				if (confirmed == JOptionPane.YES_OPTION) 
				{
					// if a bank account
					if ( txt_type_of_account.getText().equalsIgnoreCase("Bank Account") )
					{
						selected_bank_account.closeAccount();
						txt_account_status.setText("Closed");
						lbl_current_balance.setText("Php" + String.valueOf(selected_bank_account.getBalance()));
					}
					else
					{
						selected_savings_account.closeAccount();
						txt_account_status.setText("Closed");
						lbl_current_balance.setText("PhP " + String.valueOf(selected_savings_account.getBalance()));
					}
				}
				
			}
		});
		btn_close_account.setBounds(220, 530, 138, 25);
		contentPane.add(btn_close_account);
	}
	
	public void goBackToMain() {
		MainMenu mainMenu = new MainMenu();
		dispose();
	}
	
	public void showMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}


}
