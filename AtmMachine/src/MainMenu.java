import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import java.awt.Font;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JTextField;

import com.sun.xml.internal.ws.util.StringUtils;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainMenu {

	private JFrame frmAtmMachine;
	private JTextField txtChoice;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMenu window = new MainMenu();
					window.frmAtmMachine.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainMenu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frmAtmMachine = new JFrame();
		frmAtmMachine.setTitle("ATM Machine");
		frmAtmMachine.setBounds(100, 100, 322, 279);
		frmAtmMachine.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAtmMachine.getContentPane().setLayout(null);
		frmAtmMachine.setVisible(true);
		
		JLabel lblMainMenu = new JLabel("Main Menu");
		lblMainMenu.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMainMenu.setBounds(12, 13, 114, 16);
		frmAtmMachine.getContentPane().add(lblMainMenu);
		
		JLabel lblNewAccount = new JLabel("[1] New Account");
		lblNewAccount.setBounds(22, 42, 130, 16);
		frmAtmMachine.getContentPane().add(lblNewAccount);
		
		JLabel lblBalanceInquiry = new JLabel("[2] Balance Inquiry");
		lblBalanceInquiry.setBounds(22, 71, 130, 16);
		frmAtmMachine.getContentPane().add(lblBalanceInquiry);
		
		JLabel lblDeposit = new JLabel("[3] Deposit");
		lblDeposit.setBounds(22, 100, 130, 16);
		frmAtmMachine.getContentPane().add(lblDeposit);
		
		JLabel lblWithdraw = new JLabel("[4] Withdraw");
		lblWithdraw.setBounds(22, 129, 130, 16);
		frmAtmMachine.getContentPane().add(lblWithdraw);
		
		JLabel lblClientProfile = new JLabel("[5] Client Profile");
		lblClientProfile.setBounds(164, 42, 130, 16);
		frmAtmMachine.getContentPane().add(lblClientProfile);
		
		JLabel lblCloseAccount = new JLabel("[6] Close Account");
		lblCloseAccount.setBounds(164, 71, 130, 16);
		frmAtmMachine.getContentPane().add(lblCloseAccount);
		
		JLabel lblExitProgram = new JLabel("[7] Exit Program");
		lblExitProgram.setBounds(164, 100, 130, 16);
		frmAtmMachine.getContentPane().add(lblExitProgram);
		
		JButton btnGo = new JButton("Go");
		btnGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					// if no selected choice
					if ( txtChoice.getText().isEmpty() ) {
						showMessage("Please select a valid choice");
						return;
					}

					int choice = Integer.parseInt(txtChoice.getText());
					
					switch (choice) {
					case 1:
							frmAtmMachine.dispose();
							FrameNewAccount frameNewAccount = new FrameNewAccount();
							frameNewAccount.setVisible(true);
						break;
					
					case 2:
							frmAtmMachine.dispose();
							FrameBalanceInquiry frame_balance_inquiry = new FrameBalanceInquiry();
							frame_balance_inquiry.setVisible(true);
						break;
					
					case 3:
							frmAtmMachine.dispose();
							FrameDeposit frame_deposit = new FrameDeposit();
							frame_deposit.setVisible(true);
						break;
					
					case 4:
							frmAtmMachine.dispose();
							FrameWithdraw frame_withdraw= new FrameWithdraw();
							frame_withdraw.setVisible(true);
						break;
					
					case 5:
							frmAtmMachine.dispose();
							FrameClientProfile frame_client_profile = new FrameClientProfile();
							frame_client_profile.setVisible(true);
						break;
					
					case 6:
							frmAtmMachine.dispose();
							FrameCloseAccount frame_close_account = new FrameCloseAccount();
							frame_close_account.setVisible(true);
						break;

					case 7:
						frmAtmMachine.dispose();
						break;
					
					default:
						showMessage("Please enter a valid choice");
						break;
					}
					
				}
				catch (Exception exception) {
					showMessage(exception.getMessage());
				}


			}
		});
		btnGo.setBounds(223, 175, 60, 25);
		frmAtmMachine.getContentPane().add(btnGo);
		
		txtChoice = new JTextField();
		txtChoice.setBounds(164, 176, 47, 22);
		frmAtmMachine.getContentPane().add(txtChoice);
		txtChoice.setColumns(10);
		
		JLabel lblEnterYourChoice = new JLabel("Enter your Choice:");
		lblEnterYourChoice.setBounds(21, 179, 131, 16);
		frmAtmMachine.getContentPane().add(lblEnterYourChoice);
	}
	
	private void showMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}
}
