import javax.swing.JOptionPane;

import sun.print.resources.serviceui;

public class BankAccount implements IBankAccount {
	
	String account_status;
	String account_number;
    String pin_number;
    double balance;
    User user;

    public BankAccount(String account_number, String pin_number, User user) {
    	this.account_status = "Open";
    	this.account_number = account_number;
    	this.pin_number = pin_number;
    	this.user = user;
    }
    
    public String getAccountStatus() {
    	return this.account_status;
    }

    public String getAccountNumber() {
    	return this.account_number;
    }

    public String getPinNumber() {
    	return this.pin_number;
    }

	public void deposit(double value) {
		if ( value < 100) {
			this.showMessage("Input an amount higher than Php 100.00");
			return;
		}
						
		this.balance += value;
	}

	public void withdraw(double value) {
		
		if ( value < 100 ) {
			this.showMessage("Input an amount higher than Php 100.00");
			return;
		}
		
		if ( value > this.balance ) {
			this.showMessage("Input an amount less than your current balance");
			return;
		}
		
		this.balance -= value;
	}
	
    public double getBalance() {
    	return this.balance;
    }
	
	@Override
	public BankAccount closeAccount() {
		this.balance = 0;
		this.account_status = "Closed";
		return this;
	}
	
	public void showMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}
}
