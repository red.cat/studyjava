import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;

public class FrameNewAccount extends JFrame {

	private JPanel contentPane;
	private JTextField txt_last_name;
	private JTextField txt_first_name;
	private JTextField txt_middle_name;
	private JTextField txt_address;
	private JTextField txt_birth_date;
	private JTextField txt_contact_number;
	private JTextField txt_account_number;
	private JTextField txt_pin_number;
	private	JRadioButton rdoBankAccount;
	private JRadioButton rdoSavingsAccount;
	private JTextField txt_initial_deposit;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrameNewAccount frame = new FrameNewAccount();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrameNewAccount() {
		setTitle("ATM Machine: New Account");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 390, 525);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewAccountForm = new JLabel("New Account Form");
		lblNewAccountForm.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewAccountForm.setBounds(26, 13, 187, 16);
		contentPane.add(lblNewAccountForm);
		
		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLastName.setBounds(26, 55, 102, 16);
		contentPane.add(lblLastName);
		
		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFirstName.setBounds(26, 84, 102, 16);
		contentPane.add(lblFirstName);
		
		JLabel lblMiddleName = new JLabel("Middle Name:");
		lblMiddleName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMiddleName.setBounds(26, 113, 102, 16);
		contentPane.add(lblMiddleName);
		
		JLabel lblNewLabel = new JLabel("Address:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(26, 142, 102, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblBirthDate = new JLabel("Birth Date:");
		lblBirthDate.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBirthDate.setBounds(26, 171, 102, 16);
		contentPane.add(lblBirthDate);
		
		JLabel lblContactNumber = new JLabel("Contact Number:");
		lblContactNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		lblContactNumber.setBounds(26, 200, 102, 16);
		contentPane.add(lblContactNumber);
		
		txt_last_name = new JTextField();
		txt_last_name.setBounds(140, 52, 205, 22);
		contentPane.add(txt_last_name);
		txt_last_name.setColumns(10);
		
		txt_first_name = new JTextField();
		txt_first_name.setBounds(140, 81, 205, 22);
		contentPane.add(txt_first_name);
		txt_first_name.setColumns(10);
		
		txt_middle_name = new JTextField();
		txt_middle_name.setBounds(140, 110, 205, 22);
		contentPane.add(txt_middle_name);
		txt_middle_name.setColumns(10);
		
		txt_address = new JTextField();
		txt_address.setBounds(140, 139, 205, 22);
		contentPane.add(txt_address);
		txt_address.setColumns(10);
		
		txt_birth_date = new JTextField();
		txt_birth_date.setBounds(140, 168, 205, 22);
		contentPane.add(txt_birth_date);
		txt_birth_date.setColumns(10);
		
		txt_contact_number = new JTextField();
		txt_contact_number.setBounds(140, 197, 205, 22);
		contentPane.add(txt_contact_number);
		txt_contact_number.setColumns(10);
		
		JButton btn_register = new JButton("Register New Account");
		btn_register.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (validateFields()) {
					User user = new User();
					user.setLastName(txt_last_name.getText());
					user.setFirstName(txt_first_name.getText());
					user.setMiddleName(txt_middle_name.getText());
					user.setBirthDate(txt_birth_date.getText());
					user.setAddress(txt_address.getText());
					user.setContactNumber(txt_contact_number.getText());
					
					// make the BankAccount now that you have the user
					// it's either a BankAccount or a SavingsAccount
					
					String account_type;
					if ( rdoBankAccount.isSelected() ) {
						account_type = "BankAccount";
					}
					else if ( rdoSavingsAccount.isSelected() ) {
						account_type = "SavingsAccount";
					}
					else {
						JOptionPane.showMessageDialog(null, "Please select an Account type");
						return;
					}
					
					String account_number = txt_account_number.getText();
					String pin_number = txt_pin_number.getText();
					Double initial_deposit = Double.parseDouble(txt_initial_deposit.getText());

					if ( account_type == "BankAccount" ) {
						// Create a BankAccount Object using the constructor
						BankAccount bank_account = new BankAccount(account_number, pin_number, user);
						bank_account.deposit(initial_deposit);

						Globals.addBankAccount(bank_account);
						
						JOptionPane.showMessageDialog(null, "New Bank Account Added!");
						goBackToMain();
					}
					else if ( account_type == "SavingsAccount" ) {
						// Create a SavingsAccounts Object using the constructor
						SavingsAccount savings_account = new SavingsAccount(account_number, pin_number, user);
						Globals.addSavingsAccount(savings_account);
						savings_account.deposit(initial_deposit);
						
						JOptionPane.showMessageDialog(null, "New Savings Account Added!");
						goBackToMain();
					}
				}
			}
		});
		btn_register.setBounds(188, 440, 157, 25);
		contentPane.add(btn_register);
		
		JButton btnBackToMain = new JButton("Back to Main Menu");
		btnBackToMain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int confirmed = JOptionPane.showConfirmDialog(null, "Are you sure you want to go back to the main menu?", "Confirmation Dialog", JOptionPane.YES_NO_OPTION);
				
				if (confirmed == JOptionPane.YES_OPTION) 
				{
					goBackToMain();
				}
			}
		});

		btnBackToMain.setBounds(12, 440, 146, 25);
		contentPane.add(btnBackToMain);
		
		JLabel lblAccountNumber = new JLabel("Account Number:");
		lblAccountNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAccountNumber.setBounds(26, 326, 102, 16);
		contentPane.add(lblAccountNumber);
		
		JLabel lblPinNumber = new JLabel("Pin Number:");
		lblPinNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPinNumber.setBounds(26, 352, 102, 16);
		contentPane.add(lblPinNumber);
		
		txt_account_number = new JTextField();
		txt_account_number.setEditable(false);
		txt_account_number.setBounds(140, 323, 205, 22);
		contentPane.add(txt_account_number);
		txt_account_number.setColumns(10);
		// initialize the value and set it readOnly
		txt_account_number.setText(Globals.generateAccountNumber());

		
		txt_pin_number = new JTextField();
		txt_pin_number.setBounds(140, 349, 205, 22);
		contentPane.add(txt_pin_number);
		txt_pin_number.setColumns(10);
		
		JLabel lblBankInformation = new JLabel("Bank Information");
		lblBankInformation.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblBankInformation.setBounds(26, 229, 157, 16);
		contentPane.add(lblBankInformation);
		
		JLabel lblAccountType = new JLabel("Account Type:");
		lblAccountType.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAccountType.setBounds(12, 258, 114, 16);
		contentPane.add(lblAccountType);
		
		rdoBankAccount = new JRadioButton("Bank Account");
		rdoBankAccount.setBounds(140, 254, 127, 25);
		contentPane.add(rdoBankAccount);

		rdoSavingsAccount = new JRadioButton("Savings Account");
		rdoSavingsAccount.setBounds(140, 284, 127, 25);
		contentPane.add(rdoSavingsAccount);
		
		JLabel lblInitialDeposit = new JLabel("Initial Deposit:");
		lblInitialDeposit.setHorizontalAlignment(SwingConstants.RIGHT);
		lblInitialDeposit.setBounds(26, 381, 102, 16);
		contentPane.add(lblInitialDeposit);
		
		txt_initial_deposit = new JTextField();
		txt_initial_deposit.setHorizontalAlignment(SwingConstants.RIGHT);
		txt_initial_deposit.setBounds(140, 378, 205, 22);
		contentPane.add(txt_initial_deposit);
		txt_initial_deposit.setColumns(10);
	}
	
	public void goBackToMain() {
		MainMenu mainMenu = new MainMenu();
		dispose();
	}
	
	public boolean validateFields() {
		
		if ( txt_last_name.getText().isEmpty() || txt_last_name.getText() == null ) 
			return false; 

		if ( txt_first_name.getText().isEmpty() || txt_first_name.getText() == null ) 
			return false; 

		if ( txt_middle_name.getText().isEmpty() || txt_middle_name.getText() == null ) 
			return false; 

		if ( txt_address.getText().isEmpty() || txt_address.getText() == null ) 
			return false; 

		if ( txt_birth_date.getText().isEmpty() || txt_birth_date.getText() == null ) 
			return false; 

		if ( txt_contact_number.getText().isEmpty() || txt_contact_number.getText() == null ) 
			return false; 

		if ( txt_pin_number.getText().isEmpty() || txt_pin_number.getText() == null ) 
			return false; 

		if ( txt_initial_deposit.getText().isEmpty() || txt_initial_deposit.getText() == null ) 
		{
		}

		return true;
	}
	
	public showMessage(String message)
	{
		JOptionPane.showMessageDialog(null, messagef);
	}
}
